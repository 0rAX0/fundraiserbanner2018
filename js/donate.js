
/**
 * @licstart The following is the entire license notice for the
 *   JavaScript code in this page.
 *
 * Copyright (C) 2014 Zyxware Technologies.
 *
 * This JavaScript is part of the CiviCRM WCI extension for
 * CiviCRM. This JavaScript is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at
 * your option) any later version.
 *
 * The code is distributed WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * @licend The above is the entire license notice for the
 *   JavaScript code in this page
 */
var wciwidgetcode =  "\r\n<style>\r\n    .crm-wci-widget {\r\n        padding:0px;\r\n        background-color: none; \/* background color *\/\r\n    }\r\n    .crm-wci-widget-border {\r\n        border: 4px solid #BF0F0F;\r\n    }\r\n\r\n    .crm-wci-widget hr {\r\n      text-align:center;\r\n      display: block; height: 1px;\r\n      border: 0; border-top: 1px solid #BF0F0F;\r\n      margin: 1em 0; padding: 0;\r\n    }\r\n\r\n    .crm-wci-widget.thin {\r\n      width: 150px;\r\n    }\r\n\r\n    .crm-wci-widget.normal {\r\n      width: 200px;\r\n    }\r\n\r\n    .crm-wci-widget.wide {\r\n      width: 100%;\r\n      }\r\n\r\n    .crm-wci-widget-title {\r\n        font-size:14px;\r\n        padding:3px;\r\n        margin: 0px;\r\n        text-align:center;\r\n        -moz-border-radius:   4px;\r\n        -webkit-border-radius:   4px;\r\n        -khtml-border-radius:   4px;\r\n        border-radius:      4px;\r\n        color: #BF0F0F;\r\n        background-color: #FFFFFF;\r\n    } \/* title *\/\r\n\r\n    .crm-amounts {\r\n        height:1em;\r\n        \/*margin:.8em 0px;*\/\r\n        margin-left: auto;\r\n        margin-right: auto;\r\n        width:98%;\r\n        font-size:13px;\r\n    }\r\n\r\n    .crm-amount-low,\r\n    .crm-amount-high {\r\n      line-height: 30px;\r\n     }\r\n\r\n    .crm-amount-low {\r\n      float: left;\r\n      margin-left: .5em;\r\n     }\r\n\r\n    .crm-amount-high {\r\n      float: right;\r\n      margin-right: .5em;\r\n     }\r\n\r\n    .crm-percentage {\r\n        margin:0px 30%;\r\n        text-align:center;\r\n    }\r\n    .crm-amount-bar { \/* progress bar *\/\r\n        width:100%;\r\n        display:block;\r\n        border:1px solid white;\r\n        \/*-moz-border-radius:   4px;\r\n        -webkit-border-radius:   4px;\r\n        -khtml-border-radius:   4px;\r\n        border-radius:      4px;*\/\r\n        \/*text-align:left;*\/\r\n        margin-left: auto;\r\n        margin-right: auto;\r\n        background-color: none;\r\n    }\r\n    .crm-amount-fill {\r\n        background-color:#FFFFFF;\r\n        height:30px;\r\n        display:block;\r\n        \/*-moz-border-radius:   4px;\r\n        -webkit-border-radius:   4px;\r\n        -khtml-border-radius:   4px;\r\n        border-radius:      4px;*\/\r\n        text-align:left;\r\n        \r\n                  width:88.691961904762%\r\n                \r\n    }\r\n    .crm-amount-raised-wrapper {\r\n        margin-bottom:.8em;\r\n    }\r\n    .crm-amount-raised {\r\n        font-weight:bold;\r\n        color:#000;\r\n    }\r\n\r\n    .crm-logo {\r\n        text-align:center;\r\n    }\r\n\r\n    .crm-comments,\r\n    .crm-donors,\r\n    .crm-campaign {\r\n        font-size:11px;\r\n        margin-bottom:.8em;\r\n        color:#000000 \/* other color*\/\r\n    }\r\n\r\n    .pointer {\r\n      cursor: pointer;\r\n    }\r\n    .crm-wci-button {\r\n        display:block;\r\n        background-color:#CECECE;\r\n        -moz-border-radius:       4px;\r\n        -webkit-border-radius:   4px;\r\n        -khtml-border-radius:   4px;\r\n        border-radius:      4px;\r\n        text-align:center;\r\n        margin:0px 10% .8em 10%;\r\n        text-decoration:none;\r\n        color:#556C82;\r\n        padding:2px;\r\n        font-size:13px;\r\n        width:120px;\r\n        margin-left: auto;\r\n        margin-right: auto;\r\n        border: 1px solid #20538D;\r\n        border-radius: 4px 4px 4px 4px;\r\n        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 1px 1px rgba(0, 0, 0, 0.2);\r\n    }\r\n\r\n    .crm-home-url {\r\n        text-decoration:none;\r\n        border:0px;\r\n        color: \/* home page link color*\/\r\n    }\r\n\r\n    a.crm-wci-button { \/* button color *\/\r\n        background-color:#BF0F0F;\r\n    }\r\n\r\n    .crm-wci-button-inner { \/* button text color *\/\r\n        \/*padding:2px;*\/\r\n        display:block;\r\n        color:#FFFFFF;\r\n        cursor: pointer;\r\n    }\r\n    #crm_wci_image_container {\r\n      text-align: center;\r\n      padding: 10px 20px;\r\n    }\r\n    .thin #crm_wci_image {\r\n      width: 100px;\r\n    }\r\n    .normal #crm_wci_image {\r\n      width: 150px;\r\n    }\r\n    .wide #crm_wci_image {\r\n      width: 200px;\r\n    }\r\n    #newsletter_msg, #newsletter_mail, #newsletter_submit {\r\n      text-align: center;\r\n      margin: 0 auto;\r\n    }\r\n    input.btnNL, button.btnNL {\r\n       color:#FFFFFF;\r\n       background:#BF0F0F;\r\n       width:120px;\r\n       margin-top: 2px;\r\n    }\r\n    #newsletter_msg {\r\n      color:#BF0F0F;\r\n    }\r\n<\/style>\r\n\r\n\r\n<div id=\"crm_wid_1\" class=\"crm-wci-widget wide\">\r\n\r\n                                <div class=\"crm-amount-bar\"><div id=\"crm_wid_1_amt_hi\" class=\"crm-amount-high\">$525,000<\/div>\r\n        <div class=\"crm-amount-fill\" id=\"crm_wid_1_amt_fill\"><div id=\"crm_wid_1_low\" class=\"crm-amount-low\">$465,633 so far<\/div><\/div>\r\n    <\/div>\r\n        <!--<div class=\"crm-amounts\">\r\n        <div id=\"crm_wid_1_amt_hi\" class=\"crm-amount-high\">Goal: $525,000<\/div>\r\n                <div id=\"crm_wid_1_low\" class=\"crm-amount-low\">$465,633 raised<\/div>\r\n            <\/div>-->\r\n      <div id=\"crm_wid_1_donors\" class=\"crm-donors\">\r\n    <\/div>\r\n        <div id=\"crm_wid_1_comments\" class=\"crm-comments\">\r\n      \r\n    <\/div>\r\n    <div id=\"crm_wid_1_campaign\" class=\"crm-campaign\">\r\n    <\/div>\r\n        <\/div>\r\n";
// Cleanup functions for the document ready method
if ( document.addEventListener ) {
    DOMContentLoaded = function() {
        document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
        onReady();
    };
} else if ( document.attachEvent ) {
    DOMContentLoaded = function() {
        // Make sure body exists, at least, in case IE gets a little overzealous
        if ( document.readyState === "complete" ) {
            document.detachEvent( "onreadystatechange", DOMContentLoaded );
            onReady();
        }
    };
}
if ( document.readyState === "complete" ) {
    // Handle it asynchronously to allow scripts the opportunity to delay ready
    setTimeout( onReady, 1 );
}

// Mozilla, Opera and webkit support this event
if ( document.addEventListener ) {
    // Use the handy event callback
    document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );
    // A fallback to window.onload, that will always work
    window.addEventListener( "load", onReady, false );
    // If IE event model is used
} else if ( document.attachEvent ) {
    // ensure firing before onload,
    // maybe late but safe also for iframes
    document.attachEvent("onreadystatechange", DOMContentLoaded);

    // A fallback to window.onload, that will always work
    window.attachEvent( "onload", onReady );
}

function onReady( ) {
  document.getElementById("widgetwci").innerHTML = wciwidgetcode;
}
